#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

typedef struct {
  double  A[3];   /* Polynomial A coefficients */
  double  B[3];   /* Polynomial B coefficients */
  double  z1, z2; /* Filter state */
} biquad_t;

/**
 * @brief Transposed direct form II digital biquad filter
 *
 * @param[in,out]  bq         filter coefficients and state
 * @param[in]      src        input buffer
 * @param[out]     dst        output buffer
 * @param[in]      n_samples  number of samples to process
 * @param[in]      reverse    if true, process samples in reversed order
 */
static void filter(biquad_t *bq, double* src, double *dst, size_t n_samples, bool reverse)
{
  for (size_t i = 0; i < n_samples; i++) {
    size_t j = i;
    if (reverse) {
      j = n_samples - i - 1;
    }

    // The src and dst buffers might overlap
    // Save a copy of the input
    double x = src[j];

    dst[j] = bq->B[0]*x + bq->z1;
    bq->z1 = bq->B[1]*x - bq->A[1]*dst[j] + bq->z2;
    bq->z2 = bq->B[2]*x - bq->A[2]*dst[j];
  }
}

/*
 * Compute PID controller polynomial coefficients
 *
 *  INPA: RFCtrlKp
 *  INPB: RFCtrlKi (Ki*Ts)
 *  INPC: RFCtrlKd
 *  INPD: RFCtrlTf
 *  INPE: Sample time (us)
 *  INPF: Open Loop
 *
 * Output:
 *  OUTA: Polynomials A and B (length 6)
 */
static long ilc_pid_ab(aSubRecord *precord)
{
  double Kp = *((double *)precord->a);
  double Ki = *((double *)precord->b);
  double Kd = *((double *)precord->c);
  double Tf = *((double *)precord->d);
  double Ts_us = *((double *)precord->e);
  short open_loop = *((short *)precord->f);
  double *pid_AB = (double *)precord->vala;

  double Ts = Ts_us * 1e-6;
  double Nd = 1 / Tf;

  if (open_loop) {
    // A
    pid_AB[0] = 1;
    pid_AB[1] = 0;
    pid_AB[2] = 0;
    // B
    pid_AB[3] = 1;
    pid_AB[4] = 0;
    pid_AB[5] = 0;
  }
  else if (Kd == 0) {
    // PI controller
    // A
    pid_AB[0] = 1;
    pid_AB[1] = -1;
    pid_AB[2] = 0;
    // B
    pid_AB[3] = Kp;
    pid_AB[4] = Ki - Kp;
    pid_AB[5] = 0;
  }
  else {
    // PID controller
    // A
    pid_AB[0] = 1;
    pid_AB[1] = Nd*Ts - 2;
    pid_AB[2] = 1 - Nd*Ts;
    // B
    pid_AB[3] = Kp + Kd*Nd;
    pid_AB[4] = Kp*(Nd*Ts - 2) + Ki - Kd*2*Nd;
    pid_AB[5] = Kp*(1 - Nd*Ts) + Ki*(Nd*Ts - 1) + Kd*Nd;
  }

  precord->neva = 6;

  return 0;
}

/*
 * Compute Butterworth low-pass filter polynomial coefficients
 *
 *  INPA: Bw, filter bandwidth (Hz)
 *  INPB: Ts, Sample time (us)
 *
 * Output:
 *  OUTA: Polynomials A and B (length 6)
 * */
static long ilc_lpf_ab(aSubRecord *precord)
{
  double Bw = *((double *)precord->a);
  double Ts_us = *((double *)precord->b);
  double *lpf_AB = (double *)precord->vala;

  double wc = 2*M_PI*Bw;

  // Filter coefficients for second order filter
  // H(s) = 1 / (a*s^2 + b*s + c)
  double a = 1 / wc / wc;
  double b = M_SQRT2 / wc;
  double c = 1;

  double Ts = Ts_us * 1e-6; // Sample time (seconds)
  double Ts2 = Ts*Ts;       // Ts^2
  double lpf_a0 = 4*a + 2*Ts*b + Ts2*c;

  // A coefficients
  lpf_AB[0] = 1;
  lpf_AB[1] = (-8*a + 2*Ts2*c) / lpf_a0;
  lpf_AB[2] = (4*a - 2*Ts*b + Ts2*c) / lpf_a0;

  // B coefficients
  lpf_AB[3] = Ts2 / lpf_a0;
  lpf_AB[4] = 2*Ts2 / lpf_a0;
  lpf_AB[5] = Ts2 / lpf_a0;

  precord->neva = 6;

  return 0;
}

/*
 * Iterative learning control algorithm
 *
 *  INPA: Enable
 *  INPB: PID error waveform
 *  INPC: Feed forward waveform
 *  INPD: Gain
 *  INPE: ILC start (sample index)
 *  INPF: ILC end (sample index)
 *  INPG: Time shift (samples)
 *  INPH: PID controller polynomials A, B
 *  INPI: LP filter polynomials A, B
 *  INPJ: Magnitude limit
 *
 * Output:
 *  OUTA: Updated FF waveform
 *  OUTB: PID response waveform
 *  OUTC: Execution time
 *  OUTD: Data ready
 */
static long ilc_process(aSubRecord *precord)
{
  // Input
  short ilc_enabled     = *((short *)precord->a);
  double *pid_error     = (double *)precord->b;
  double *ff_table      = (double *)precord->c;
  double ilc_gain       = *((double *)precord->d);
  epicsUInt32 ilc_start = *((epicsUInt32 *)precord->e);
  epicsUInt32 ilc_end   = *((epicsUInt32 *)precord->f);
  epicsInt32 time_shift = *((epicsInt32 *)precord->g);

  double *pid_ab = (double *)precord->h;
  double *lpf_ab = (double *)precord->i;
  double limit = *((double *)precord->j);

  // Output
  double *ff_new   = (double *)precord->vala;
  double *pid_rsp  = (double *)precord->valb;
  double *duration = (double *)precord->valc;
  short  *ready    = (short  *)precord->vald;

  size_t n_samples = precord->neb;
  size_t ff_size =  precord->nec;

  struct timespec start, end;
  *ready = 0;

  if (!ilc_enabled)
  {
    return 1;
  }

  if (precord->neb == 0 || precord->nec == 0)
  {
    return 1;
  }

  /* pid_ab and lpf_ab must be six elements */
  if (precord->neh < 6 || precord->nei < 6)
  {
    printf("calc_ilc() Size mismatch: neh = %u, nei = %u\n", precord->neh, precord->nei);
    return 1;
  }

  if (ilc_end < ilc_start)
  {
    printf("calc_ilc() Bad window size: start= %u, end = %u\n", ilc_start, ilc_end);
    return 1;
  }

  clock_gettime(CLOCK_MONOTONIC, &start);

  if (ilc_start > n_samples) {
    ilc_start = 0;
  }

  if (ilc_end > n_samples) {
    ilc_end = n_samples;
  }

  size_t window_size = ilc_end - ilc_start;
  precord->neva = n_samples;
  precord->nevb = window_size;

  // If FF table is shorter than input waveform, extend with last element
  double fill = ff_table[ff_size - 1];
  for (size_t i=ff_size; i<n_samples; i++) {
    ff_table[i] = fill;
  }

  // PID controller response
  biquad_t pid_bq = {
    .A = {pid_ab[0], pid_ab[1], pid_ab[2]},
    .B = {pid_ab[3], pid_ab[4], pid_ab[5]},
    .z1 = 0,
    .z2 = 0,
  };

  filter(&pid_bq, &pid_error[ilc_start], pid_rsp, window_size, false);

  if (time_shift > 0) {
    // Shift left
    size_t abs_shift = time_shift;
    if (abs_shift < window_size) {
      memmove(pid_rsp, pid_rsp + abs_shift, (window_size - abs_shift)*sizeof(double));
    }
    else {
      abs_shift = window_size;
    }
    // Insert zeros at end of window
    for (size_t i = window_size - abs_shift; i < window_size; i++) {
      pid_rsp[i] = 0.0;
    }
  }
  else if (time_shift < 0) {
    // Shift right
    size_t abs_shift = -time_shift;
    if (abs_shift < window_size) {
      memmove(pid_rsp + abs_shift, pid_rsp, (window_size - abs_shift)*sizeof(double));
    }
    else {
      abs_shift = window_size;
    }
    // Insert zeros at beginning of window
    for (size_t i = 0; i < abs_shift; i++) {
      pid_rsp[i] = 0.0;
    }
  }

  // Copy FF table
  memcpy(ff_new, ff_table, n_samples*sizeof(double));

  // Pointer to ILC window in output buffer
  double *window = &ff_new[ilc_start];

  // Add PID controller response
  for (size_t i = 0; i < window_size; i++) {
    window[i] += ilc_gain * pid_rsp[i];
  }

  // Low pass filter
  double z2 = window[0] * (lpf_ab[5] - lpf_ab[2]);      // x[0] * (b[2] - a[2])
  double z1 = window[0] * (lpf_ab[4] - lpf_ab[1]) + z2; // x[0] * (b[1] - a[1]) + z2

  biquad_t lpf_bq = {
    .A = {lpf_ab[0], lpf_ab[1], lpf_ab[2]},
    .B = {lpf_ab[3], lpf_ab[4], lpf_ab[5]},
    .z1 = z1,
    .z2 = z2,
  };

  // Zero-phase filter. Forward direction
  filter(&lpf_bq, window, window, window_size, false);
  // Zero-phase filter. Reverse direction
  filter(&lpf_bq, window, window, window_size, true);

  // Magnitude limit
  for (size_t i = 0; i < window_size; i++) {
    if (window[i] > limit)
      window[i] = limit;
    else if (window[i] < -limit)
      window[i] = -limit;
  }

  // Measure execution time
  clock_gettime(CLOCK_MONOTONIC, &end);
  *duration = (end.tv_sec - start.tv_sec) + (end.tv_nsec - start.tv_nsec) * 1e-9;

  *ready = 1;

  return 0;
}

epicsRegisterFunction(ilc_pid_ab);
epicsRegisterFunction(ilc_lpf_ab);
epicsRegisterFunction(ilc_process);
