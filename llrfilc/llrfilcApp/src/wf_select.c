#include <stddef.h>
#include <string.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>

/*
 * Select between two waveforms
 *
 * Parameters
 * INPA: waveform A
 * INPB: waveform B
 * INPC: select
 *
 * Output
 * OUTA: copy of waveform A if select == 0, else copy of waveform B
 */
static long wf_select(aSubRecord *precord)
{
  short select = *((short *)precord->c);
  double *src;
  size_t size;

  if (select == 0) {
    src = (double *)precord->a;
    size = precord->nea;
  }
  else {
    src = (double *)precord->b;
    size = precord->neb;
  }

  precord->neva = size;
  memcpy(precord->vala, src, size*sizeof(double));

  return 0;
}

epicsRegisterFunction(wf_select);
